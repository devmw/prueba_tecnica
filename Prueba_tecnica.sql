-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 19-07-2019 a las 20:51:34
-- Versión del servidor: 10.1.34-MariaDB
-- Versión de PHP: 7.0.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `Prueba_tecnica`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Cliente`
--

CREATE TABLE `Cliente` (
  `IdCliente` int(11) NOT NULL,
  `PrimerNombre` varchar(45) NOT NULL,
  `SegundoNombre` varchar(45) NOT NULL,
  `PrimerApellido` varchar(45) NOT NULL,
  `SegundoApellido` varchar(45) NOT NULL,
  `Telefono` varchar(15) NOT NULL,
  `Direccion` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `Cliente`
--

INSERT INTO `Cliente` (`IdCliente`, `PrimerNombre`, `SegundoNombre`, `PrimerApellido`, `SegundoApellido`, `Telefono`, `Direccion`) VALUES
(1, 'Ruben', 'Dario', 'Sotelo', 'Carrillo', '3187985671', 'Transversal 1D Norte'),
(2, 'Juan', 'Alfredo', 'Gomez', 'Vargas', '3124453453', 'Diagonal 5Fb');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Compra`
--

CREATE TABLE `Compra` (
  `IdCompra` int(11) NOT NULL,
  `IdCliente` int(11) NOT NULL,
  `Fecha` date NOT NULL,
  `TotalCompra` double NOT NULL,
  `CompraId` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `Compra`
--

INSERT INTO `Compra` (`IdCompra`, `IdCliente`, `Fecha`, `TotalCompra`, `CompraId`) VALUES
(15, 2, '2019-07-18', 44800, '80530928-25263721'),
(16, 1, '2019-07-18', 59800, '52926717-23243968'),
(17, 2, '2019-07-18', 44900, '2680682-62764779'),
(18, 2, '2019-07-18', 24900, '39690200-61182101'),
(19, 2, '2019-07-18', 44900, '80597264-18723192'),
(20, 2, '2019-07-18', 39900, '30234139-69697428'),
(21, 2, '2019-07-18', 44900, '54664064-98754216'),
(22, 2, '2019-07-18', 20000, '11269238-95497445'),
(23, 2, '2019-07-18', 0, '79591630-16814976'),
(24, 2, '2019-07-18', 44900, '73848931-85505188'),
(25, 2, '2019-07-18', 20000, '36399056-66346466'),
(26, 2, '2019-07-18', 25000, '24751463-96150498'),
(27, 2, '2019-07-18', 25000, '98159170-11335537');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `DetalleCompra`
--

CREATE TABLE `DetalleCompra` (
  `IdDetalleCompra` int(11) NOT NULL,
  `IdCompra` int(11) NOT NULL,
  `IdProducto` int(11) NOT NULL,
  `Cantidad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `DetalleCompra`
--

INSERT INTO `DetalleCompra` (`IdDetalleCompra`, `IdCompra`, `IdProducto`, `Cantidad`) VALUES
(12, 24, 73848931, 0),
(13, 25, 36399056, 0),
(14, 26, 24751463, 0),
(15, 27, 98159170, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Producto`
--

CREATE TABLE `Producto` (
  `IdProducto` int(11) NOT NULL,
  `Nombre` varchar(250) NOT NULL,
  `ImagenUrl` longtext NOT NULL,
  `Descripcion` varchar(500) NOT NULL,
  `Precio` double NOT NULL,
  `cantidadExistencia` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `Producto`
--

INSERT INTO `Producto` (`IdProducto`, `Nombre`, `ImagenUrl`, `Descripcion`, `Precio`, `cantidadExistencia`) VALUES
(1, 'Camisa', 'https://cdn.pixabay.com/photo/2015/07/14/16/33/white-845071_960_720.jpg', 'Camisa manga larga XS', 20000, 20),
(2, 'Medias 5x3', 'https://cdn.pixabay.com/photo/2019/07/11/08/19/sock-4330279_960_720.jpg', 'Medias tobilleras 3x5', 5000, 10),
(3, 'Camisa manga corta', 'https://cdn.pixabay.com/photo/2015/07/14/16/33/white-845071_960_720.jpg', 'Camisa corta', 19900, 10),
(4, 'Medias tobilleras', 'https://cdn.pixabay.com/photo/2019/07/11/08/19/sock-4330279_960_720.jpg', 'Medias tobilleras 5x5', 5000, 5);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `Cliente`
--
ALTER TABLE `Cliente`
  ADD PRIMARY KEY (`IdCliente`);

--
-- Indices de la tabla `Compra`
--
ALTER TABLE `Compra`
  ADD PRIMARY KEY (`IdCompra`),
  ADD KEY `clienteXcompra` (`IdCliente`);

--
-- Indices de la tabla `DetalleCompra`
--
ALTER TABLE `DetalleCompra`
  ADD PRIMARY KEY (`IdDetalleCompra`),
  ADD KEY `detalleXcompra` (`IdCompra`),
  ADD KEY `detalleXproducto` (`IdProducto`);

--
-- Indices de la tabla `Producto`
--
ALTER TABLE `Producto`
  ADD PRIMARY KEY (`IdProducto`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `Cliente`
--
ALTER TABLE `Cliente`
  MODIFY `IdCliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `Compra`
--
ALTER TABLE `Compra`
  MODIFY `IdCompra` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT de la tabla `DetalleCompra`
--
ALTER TABLE `DetalleCompra`
  MODIFY `IdDetalleCompra` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `Producto`
--
ALTER TABLE `Producto`
  MODIFY `IdProducto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `Compra`
--
ALTER TABLE `Compra`
  ADD CONSTRAINT `clienteXcompra` FOREIGN KEY (`IdCliente`) REFERENCES `Cliente` (`IdCliente`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `DetalleCompra`
--
ALTER TABLE `DetalleCompra`
  ADD CONSTRAINT `detalleXcompra` FOREIGN KEY (`IdCompra`) REFERENCES `Compra` (`IdCompra`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
